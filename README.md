_Forked from https://github.com/pcvonz/tsl2591-rs._

# TSL2591 driver 
A WIP Rust implementation of the TSL2591 High Dynamic Range Digital Light Sensor.

Most of what's here is a straight port of the [Adafruit C++ library](https://github.com/adafruit/Adafruit_TSL2591_Library).

## Features
- [x] Basic reading and lux calculation
- [ ] Interrupt support

## Usage
```rust
t.enable().unwrap();
loop {
    let (ch_0, ch_1) = t.get_channel_data().unwrap();
    delay.delay_ms(200); // wait for sensor to integrate data
    let d = t.calculate_lux(ch_0, ch_1).unwrap();
    println!("Counts {}", d);
}
```

## License
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitlab.com/scrobotics/embedded-rs/tsl2591-rs/-/blob/master/LICENSE)

This tool is released under the MIT license, hence allowing commercial use of the library. Please refer to the [LICENSE](https://gitlab.com/scrobotics/embedded-rs/tsl2591-rs/-/blob/master/LICENSE) file.
